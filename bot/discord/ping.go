package discord

import "github.com/bwmarrin/discordgo"

func (b *Bot) Ping(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {
	_, err := b.RespondToEvent(s, m, i, "Pong!", true)
	return err
}
