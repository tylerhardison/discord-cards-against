I'll tell you one thing, it's a cruel, cruel world. 
I don't look ahead. I'm right here with you. It's a good way to be. 
I've been to the Leaning Tower of Pisa. It's a tower, and it's leaning. You look at it, but nothing happens, so then you look for someplace to get a sandwich. 
People come into your lives who you have a good time with, and time goes by and you still have a good time with them and you do stupid stuff with them. To me, that's life. 
It's fun to be on the edge. I think you do your best work when you take chances, when you're not safe, when you're not in the middle of the road, at least for me, anyway. 
Of course I've got lawyers. They are like nuclear weapons, I've got em 'cause everyone else has. But as soon as you use them they screw everything up. 
I love women. I mean, I'm married, but I used to get in all kinds of trouble. 
So, you pick this stuff here and this stuff there and then you see things in certain ways and you start visualizing and thank God I get the chance to do this. It's really the greatest thing in the whole wide world. 
There are two dilemmas that rattle the human skull: How do you hang on to someone who won't stay? And how do you get rid of someone who won't go? 
I am self-centred. I just adore myself. 
I don't know. I think it's funny! I think it's funny! I go, what? It's so absurd. I'm alone. 
I'm shooting in Brooklyn, we've got all kinds of crap going on, and I'm all alone now in a big hotel suite that you can't believe the size of it and a thing sticks in my foot and I just think it's the funniest thing that's ever happened to me. 
When I'm directing or producing, I like to do different kinds of stories. But you have to know what you really like, and when you do, you have to front your position. 
In this movie, you have all the things you love from Tim. All the magic and the whimsy and the surreal, but he also has a fantastic story of a father and son that really gets under your skin. 
If you're going to have kids, there's only one way to go. They have to know they're the most important things in your life, and once you're doing that, there's no way that you could not learn from them, because they just give you stuff constantly. 
My parents worked their tails off, but we weren't the poorest people in town. Some people I went to school with, you could tell they were dirt poor. 
Jersey is always with me. I was one of the lucky ones. Asbury Park is just the greatest place in the world to spend your childhood. 
What you do with it and things like that, but I basically chose this after I read it because I thought it was different and funny and unique and dark - things that I like to do. 
I don't think I've been bored, ever. I've always been working on two or three things at a time; whether it was in the early days, or whatever, I was always working on something. 
I'm always studying and I've been doing it for a long time now. 
I'll watch Ricky Gervais in anything he does. The guy's hilarious. 
After you've lived with somebody for 11 years, what's a guy in a robe reading from a book going to change? 
So we see Edward as a young man on the road and he meets a giant and he brings the giant to a circus where he meets a Miss Calloway. He sees the woman of his dreams and I am the only one who knows who she is. 
I developed that for a long time. I also developed 'Sugar Sweet Science' at New Line and that didn't happen. That was a boxing movie. And between all that there were a couple of other things. 
I didn't do it because of the underlying greed that's prevailing, but it is about greed, doing the right thing at the right time using your clout when you have it and what for and what reason. 
I lay on the ground, but then I can't reach - I don't want to take my foot out of the tub - but I've got to call somebody because I've got to get a band-aid or something to stop the bleeding. 
It's a first for me. It's my first nude scene. 
It's just an ice bucket with a bottle in it. The two flute glasses are little tray. I got to shut the curtains. I'm in my boxer shorts and shirt. I'm going to take a bath and go to bed. But I want to shut the blinds so it's really dark in the room. 
The son has always felt like he was a footnote in one of the stories the father tells. The father is an amazing storyteller and one of the tales that he tells is how he met his wife. 
This big part flies off on the floor. The other part goes like this and lands in my foot! Standing up! It's standing in my foot! Right in the side of my foot. The flute glass. I think I'm like in one of my own pictures. 
We did 'Erin Brockovich', we did 'Man on the Moon', we did 'Living Out Loud', but now I'm going to keep going. 
Well, you know, I feel like it's about a lot of things. The reason that I made it was because I thought it was really funny and unique and just a different genre. 
What Tim does is, he calls me and sends me the script. And then he sends me a drawing, an illustration of his image of me as the character. It's so great. 
Most men somewhere in their psyche are still dragging women around by their hair. It's terrible. I have two daughters, but even before my kids were born I always thought that it was terrible. 
Wherever I am, it's a really good feeling to have that connection to people. I love to go out to talk to people and be with folks. I don't shy away from it. 
I have been very fortunate in my life. I have had a lot of happiness. I have a great family and I work a lot, and that's what I like to do. 
You have to give people permission to laugh. That's why they would always cut to the banana peel in the Laurel and Hardy movies. 
People come into your lives who you have a good time with, and time goes by and you still have a good time with them and you do stupid stuff with them. To me, that's life.
A tree falls the way it leans.  Be careful the way you lean.
We could be in a turtle's dream in outer space.
I'll tell you one thing, it's a cruel, cruel world.
I don’t look ahead. I’m right here with you. It’s a good way to be.
It’s fun to be on the edge.
School? It's out of the question. Who would be here to sign for the packages?
I am self-centred. I just adore myself.
Most men somewhere in their psyche are still dragging women around by their hair.
You could be a boxer or something. I could be your manager.
I've been to the Leaning Tower of Pisa. It's a tower, and it's leaning.
There are two dilemmas that rattle the human skull: How do you hang on to someone who won't stay? And how do you get rid of someone who won't go?
I'll watch Ricky Gervais in anything he does. The guy's hilarious.
What do you call 500 lawyers lying on the bottom of the ocean? A good start.
A lot of it revolves around food with me because a lot of my life does in a way.
Well, that's it. The last check is written. What do we have left out of the $50,000 reward?
In a manner of speaking, yes. Uh, welcome to Wormwood Motors. Harry Wormwood, owner, founder, whatever.
The son has always felt like he was a footnote in one of the stories the father tells.
Dinner time is family time. What is this trash you're reading?
What car? Sued by who? Who you been talking to?
I didn't go to college. I don't know anybody who did. Bunch of hippies and cesspool salesmen, ha ha ha ha...
Yes. You tend the elephants. Make those ears disappear!
There are only three things with that kind of unconditional acceptance: Dogs, donuts, and money.
What you do with it and things like that, but I basically chose this after I read it because I thought it was different and funny and unique and dark - things that I like to do.
The reason that I made it was because I thought it was really funny and unique and just a different genre.
We did 'Erin Brockovich', we did 'Man On The Moon', we did 'Living Out Loud', but now I'm going to keep going.
Wherever I am, it's a really good feeling to have that connection to people.
Since you're an educator, I'll make you a deal.
So, you pick this stuff here and this stuff there and then you see things in certain ways and you start visualizing and thank God I get the chance to do this.
Uh... Webster - calling from California. I happen to have something, uh, some merchandise, that you, uh, that you might want.
You have until tomorrow night to fix that!
When I’m directing or producing, I like to do different kinds of stories.
I climbed like a billion stairs... it's not like I can take them two at a time.
I did not glue my hat to my head! The hat shrunk!
All the magic and the whimsy and the surreal, but he also has a fantastic story of a father and son that really gets under your skin.
I'm always studying and I've been doing it for a long time now.
I was raised on the Marx Brothers and the Three Stooges, which is a little cruel. It's kind of like, in a way, dark.
Christmas is a good excuse to eat everything.
Life is a game, whoever has the most money at the end wins.
I don't know. I think it's funny!... It's so absurd. I'm alone.
Julius, don't do this to me! I got a car to deliver! The last thing I need is a detour to the slammer!
A book? What do you want a book for?
Of course I've got lawyers. They are like nuclear weapons.
Of course you didn't do it, you little twit.
But instead of just one perfect kid, Mom had the two of us.
Look, there's no way Mr. Beetroot, that I am gonna deliver any damn Cadillac, unless I know for sure that I have... twenty for my cut.
The moment I sat down I thought I was looking into a mirror.
My life just got flushed down the toilet.
We set the building on fire, you just happened to catch on fire!
If you're being smart with me, young lady, you're going to be punished.
I’m right, you're wrong, and there's nothing you can do about it.
You can change all the laws you want. You can't stop the game.
There is no winning! Only degrees of losing!
There's nothing you can get from a book that you can't get from a television faster.
You're a Wormwood, you start acting like one! Now sit up and look at the TV.
Never do anything I tell you, without checking with me first.
You’re making people happy. That’s no small thing.
Yeah, I got a boy, Mikey, and one mistake, Matilda.
I robbed a thief! How can you not see the humor in that?
Getting old is a gift.
Welcome to the Medici Family circus where we believe no wild animals shall be held in captivity.
The whole point of love is to put someone else’s needs above your own.
The choices we make, dictate the lives we lead.
We're living in a country that discriminates, and has certain racist tendencies.
A word to the wise ain't necessary - it's the stupid ones that need the advice.
I'll tell you one thing. I love Philadelphia.
I developed that for a long time. I also developed 'Sugar Sweet Science' at New Line and that didn't happen. That was a boxing movie. And between all that there were a couple of other things.
$5,000? I'm not paying it! What are they going to do, repossess the kid?
[It] gives kids a chance to have a great time in the summer, and act like kids.
You know what a Rainmaker is, kid? The bucks are gonna be falling from the sky.
Even when you read the script of 'Big Fish', which is really a terrific script, you don't really get into the world that he's creating until you take that step with him, that first step into a world he's created in his mind.
You have to give people permission to laugh.
In order for the light of gratitude and happiness to shine so brightly, the darkness of how it could be worse must be present.
Jersey is always with me. I was one of the lucky ones. Asbury Park is just the greatest place in the world to spend your childhood.
I didn't do it because of the underlying greed that's prevailing, but it is about greed, doing the right thing at the right time using your clout when you have it and what for and what reason.
I think of all the characters that I've ever played, they're always about five feet tall.
I think 'Taxi' is one of the seminal, great shows that was ever made on television.
From now on, this family does exactly what I say, when exactly, when I say it!
Guess which newly-made judge was assigned to Great Benefit's case?
A bad salesman will automatically drop his price. Bad salesmen make me sick.
What a waste of time!
Edward's the kind of actor who is always nosing into it, trying to find it, looking for it, and searching and Robin is, too, but in his own way.
I've always been working on two or three things at a time; whether it was in the early days, or whatever, I was always working on something.
Yep. Croaked with a heart attack, dropped dead by his swimming pool.
If it’s true journalism, you shouldn’t be paying for it.