package discord

import (
	"context"
	"log"

	"github.com/bwmarrin/discordgo"
	"go.mongodb.org/mongo-driver/bson"
)

func (b *Bot) JoinGuild(s *discordgo.Session, m *discordgo.GuildCreate) {
	if m.Guild.Unavailable {
		return
	}

	log.Printf("Joined guild %s", m.Name)

	_, err := s.ApplicationCommandBulkOverwrite(s.State.User.ID, m.ID, b.GuildCommands)

	if err != nil {
		log.Printf("Error overwriting guild commands: %s", err.Error())
	}

	b.AnnounceBotOnline(s, m)
}

func (bot *Bot) AnnounceBotOnline(s *discordgo.Session, guild *discordgo.GuildCreate) {
	// Read in the ../releasenotes.json file and send it to the bot-testing channel

	// Generate a MessageEmbedField containing the list of release notes

	// Loop through each guild in the session

	guildDatabase := bot.MongoClient.Database(ConvertGuildNameToDBName(guild.Name))
	guildPreferencesCollection := guildDatabase.Collection("preferences")

	var guildPreferences bson.M

	err := guildPreferencesCollection.FindOne(context.Background(), bson.M{}).Decode(&guildPreferences)

	if err != nil {
		// Set the default preferences
		guildPreferences = bson.M{
			"announce_channel":     "bot-testing",
			"startup_announcement": true,
			"activation_sigil":     "^",
		}

		_, err := guildPreferencesCollection.InsertOne(context.Background(), guildPreferences)

		if err != nil {
			log.Printf("Announce: Error inserting default preferences: %s", err.Error())
		}
	}

	if bot.GuildSigils == nil {
		bot.GuildSigils = make(map[string]string)
	}

	bot.GuildSigils[guild.ID] = guildPreferences["activation_sigil"].(string)

	// Get channels for this guild
	log.Printf("Announce: Guild: %s? %+v", guild.Name, guildPreferences)

	// Check if the guild has the startup announcement enabled
	var shouldAnnounce bool

	// Attempt to assert the value as bool directly.
	if val, ok := guildPreferences["startup_announcement"].(bool); ok {
		shouldAnnounce = val
	} else if val, ok := guildPreferences["startup_announcement"].(string); ok {
		// If the value is a string, check if it represents "false".
		shouldAnnounce = val != "false"
	} else {
		// Default to true if the value is neither bool nor string, or if it's nil.
		shouldAnnounce = true
	}
	

	if shouldAnnounce {
		channels := guild.Channels

		log.Printf("Announce: Channels: %+v", channels)
		for _, c := range channels {
			// Check if channel is a guild text channel and not a voice or DM channel
			if c.Type != discordgo.ChannelTypeGuildText {
				continue
			}

			if c.Name != guildPreferences["announce_channel"].(string) && c.ID != guildPreferences["announce_channel"].(string) {
				continue
			}

			err := bot.GetAboutInformation(s, nil, nil, []string{c.ID, guild.ID})

			if err != nil {
				log.Printf("Announce: Error sending message: %s", err)
			}

		}
	}
}
