# Contributing to the bot.

## How to contribute

1. Fork the repository
2. Clone your fork
3. Create a new branch
4. Make your changes
5. Commit and push your changes
6. Create a pull request (gitlab)
7. Wait for your pull request to be reviewed and merged

