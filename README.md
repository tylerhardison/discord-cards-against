<!--
    This is the README.md file.
    It's used to explain your bot to other users
    Include information as:
      - why should someone use your bot
      - does your bot has his own website?
      - does your bot have documentation?
    To write the REAMDE.md yoou can use Markodwn
    syntax or html syntax.
-->
<h1 align="center">Humans Against Waxed Paper Rectangles</h1>
<!--
    Modify using this format:
    <img alt="Name of the image" src="path to the image">
-->
<p align="center">
  <a href="https://discord.com/oauth2/authorize?client_id=1174383107563061299&permissions=534723951680&redirect_uri=https%3A%2F%2Fdiscord.com%2Fapi%2Foauth2%2Fauthorize&scope=bot">
    <img alt="Logo example" src="logo/cardsbot_f.jpg">
  </a>
</p>

### You can invite the bot to your server using [this](https://discord.com/oauth2/authorize?client_id=1174383107563061299&permissions=534723951680&redirect_uri=https%3A%2F%2Fdiscord.com%2Fapi%2Foauth2%2Fauthorize&scope=bot) link.

#### For contributing to the bot's code, read the [CONTRIBUTING.md](CONTRIBUTING.md) file.

---

### Table of contents

- [Idea](#idea)
- [Commands](#commands)
- [Compiling from source](#compiling-from-source)
- [License](#license)

---

### Idea

This bot is a new implementation of the [Cards Against Humanity](https://cardsagainsthumanity.com/) game. I took it on as a project because it seemed like a fun idea to implement and I wanted to learn more about the Discord API. The bot is written in Golang and uses the [discordgo](https://github.com/bwmarrin/discordgo) library. The bot is hosted on a Intel NUC running Debian and k3s.

---

### Commands

All the commands start with the prefix `^`

| Name               | Description                                             |
| ------------------ | ------------------------------------------------------- |
| ping               | Pong                                                    |
| horoscope <phrase> | get a horoscope colored by your chosen phrase.          |
| weather <location> | get weather for a location                              |
| cah start          | start a new game of Cards Against Humanity              |
| devito             | get a random Danny Devito quote                         |
| selftest           | run a selftest to check if the bot is working correctly |
| help               | Get help about a command or the bot itself              |

---

### License

<!--
  By default this project is under the mit license.

    https://opensource.org/licenses/MIT

  You can change it in the "LICENSE" file of your
  repository.
-->

This project is under the [MIT License](LICENSE)

### Contributors
@nslee333
