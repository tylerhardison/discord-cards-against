package discord

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/bwmarrin/discordgo"
)

func (b *Bot) GetAboutInformation(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {

	channelId := ""
	guildId := ""
	if m != nil {
		channelId = m.ChannelID
		guildId = m.GuildID
	} else if i != nil {
		channelId = i.ChannelID
		guildId = i.GuildID
	}

	guildName := ""

	if channelId == "" {
		channelId = arguments[0]
		guildName = arguments[1]
	}

	// Get my name from Discord
	user, err := s.User("@me")

	if err != nil {
		log.Printf("About: Error getting user: %s", err.Error())
	}

	botName := user.Username

	if guildName == "" {
		guild, err := s.State.Guild(guildId)

		if err != nil {
			log.Printf("About: Error getting guild: %s", err.Error())
			return err
		}

		guildName = guild.Name
	}

	preferencesData, err := b.GetServerPreferences(s, guildName)

	if err != nil {
		log.Printf("About: Error getting server preferences: %s", err.Error())
	}

	commandSigil := preferencesData["activation_sigil"]

	releaseNotesEmbed := b.GetReleaseNotes()

	embed := &discordgo.MessageEmbed{
		Title:       fmt.Sprintf("%s is now online!", botName),
		Description: fmt.Sprintf("%s is now online and ready to serve you funny horoscopes and play games!", botName),
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:  "Commands",
				Value: fmt.Sprintf("%shelp - For a list of supported commands.", commandSigil),
			},
			&releaseNotesEmbed,
			{
				Name:  "Like the Bot?",
				Value: "If you like the bot, consider [buying me a coffee](https://www.buymeacoffee.com/yellerjeep) or [supporting me on Patreon](https://www.patreon.com/yellerjeep_tr).",
			},
			{
				Name:  "Want this bot in your server?",
				Value: "You can add this bot to your server by [clicking here](https://discord.com/api/oauth2/authorize?client_id=1174383107563061299&permissions=18685256133696&scope=bot).",
			},
			{
				Name:  "Want to help develop this bot?",
				Value: "You can find the source code for this bot on [GitLab](https://gitlab.com/tylerhardison/discord-cards-against).",
			},
			{
				Name:  "Have an issue or bug report?",
				Value: "You can report issues or bugs on [GitLab](https://gitlab.com/tylerhardison/discord-cards-against/-/issues). Note you can also add a #bot-testing channel to your server to see when restarts happen and release notes.",
			},
		},

		Color: 0x00ff00,
	}

	messageSend := discordgo.MessageSend{
		Embed: embed,
	}

	// Send announcement message
	_, err = s.ChannelMessageSendComplex(
		channelId,
		&messageSend,
	)
	if err != nil {
		log.Printf("Announce: Error sending message: %s", err)
	}
	return err
}

func (b *Bot) GetReleaseNotes() discordgo.MessageEmbedField {

	releaseNotesString, err := os.ReadFile("../releasenotes.json")
	if err != nil {
		log.Printf("Announce: Error reading release notes: %s", err)
	}

	var releaseNotes ReleaseNotes

	err = json.Unmarshal(releaseNotesString, &releaseNotes)

	if err != nil {
		log.Printf("Announce: Error unmarshalling release notes: %s", err)
	}

	var releaseNotesEmbed = discordgo.MessageEmbedField{
		Name:  "Release Notes",
		Value: "",
	}

	for index, releaseNote := range releaseNotes {
		// parse the date, we only want release notes newer than 24 hours.
		// if the date is older than 24 hours, skip it

		date, err := time.Parse("2006-01-02T15:04:05.999999Z07:00", releaseNote.Timestamp)
		if err != nil {
			log.Printf("Announce: Error parsing release note date: %s", err)
		}

		if time.Since(date).Hours() > 24 && index != 0 {
			continue
		}

		releaseNotesEmbed.Value += fmt.Sprintf("## %s\n", releaseNote.Version)

		for _, note := range releaseNote.Notes {
			releaseNotesEmbed.Value += fmt.Sprintf("%s - (Author: <@%s>) %s\n", emojiMap[note.Type], note.Author, note.Message)
		}

		// Add a horizontal rule between release notes unless its the last one
		if index != len(releaseNotes)-1 {
			releaseNotesEmbed.Value += "----------------------------------------\n"
		}
	}

	if len(releaseNotesEmbed.Value) > 1024 {
		releaseNotesEmbed.Value = releaseNotesEmbed.Value[:1024]
	}
	return releaseNotesEmbed
}

type ReleaseNote struct {
	Version   string `json:"version"`
	Timestamp string `json:"timestamp"`
	Notes     []struct {
		Type    string `json:"type"`
		Message string `json:"message"`
		Author  string `json:"author"`
	} `json:"notes"`
}

type ReleaseNotes []ReleaseNote

var emojiMap = map[string]string{
	"feature":     ":star:",
	"bug":         ":bug:",
	"fix":         ":wrench:",
	"change":      ":gear:",
	"maintenance": ":hammer:",
}
