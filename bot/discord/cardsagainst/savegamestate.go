// At the end of each round, we're going to save the game state to
// mongodb. This will allow us to recover the game state in case of a
// crash or restart.
package cardsagainst

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func (c *CardsAgainst) SaveGameState(context context.Context) error {
	// Save the game state to the database
	c.Mongo.
		Collection("games").
		ReplaceOne(
			context,
			bson.M{"_id": c.Guild.ID},
			bson.M{
				"_id":       c.Guild.ID,
				"gamestate": c.GameState,
				"cards":     c.Cards,
			},
			options.Replace().SetUpsert(true))

	return nil
}

// LoadGameState loads the game state from the database
func (c *CardsAgainst) LoadGameState(context context.Context) error {
	// Load the game state from the database
	result := c.Mongo.
		Collection("games").
		FindOne(context, bson.M{"_id": c.Guild.ID})

	if result.Err() != nil {
		return result.Err()
	}

	var savedGame SavedGame
	err := result.Decode(&savedGame)
	if err != nil {
		return err
	}

	// Check that the GameState was in a playing state
	if savedGame.GameState.CurrentState == None {
		return nil
	}

	// We cannot resume a game, at all, yet.
	// So we'll announce to the channel sorry for the crash
	// and reset the game state

	_, err = c.SendResponse(nil, nil, nil, []string{"Sorry, the game crashed. We're resetting the game."}, true)
	if err != nil {
		return err
	}

	return nil
}

type SavedGame struct {
	ID        string `bson:"_id"`
	GameState GameState
	Cards     Cards
}
