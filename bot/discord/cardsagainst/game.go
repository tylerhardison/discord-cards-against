package cardsagainst

import (
	"bytes"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

// This is a go routine that keeps the game state flowing, it's the main game loop
// Players have one minute to select a card, if they don't, they are skipped
// Once the all of the players have submitted a card, the czar has 60 seconds to pick a winner

// This is the main game loop

func (c *CardsAgainst) GameLoop(s *discordgo.Session, channelID string) {
	// Set the game state to game starting
	c.GameState.CurrentState = GameStarting

	_, err := s.ChannelMessageSend(channelID, "Game started!")
	if err != nil {
		log.Printf("Game Start: Error sending message: %v", err)
	}

	// Set up the master timer to progress game state. Timer is set for 60 seconds to start with
	currentTimer := time.NewTicker(1 * time.Second)

	c.GameState.Playing = true

	go func() {
		for range currentTimer.C {
			log.Printf("Tick:\n Current State: %s\n Next Transition: %s\n", c.GameStatusToEnglish(), c.GameState.NextTransition.Format(time.RFC3339))
			// Timer has expired
			// Check the game state and take action if needed
			if int(time.Until(c.GameState.NextTransition).Seconds()) >= 0 {
				description := fmt.Sprintf("Time remaining: %d seconds", int(time.Until(c.GameState.NextTransition).Seconds()))

				if c.GameState.CurrentBlackCard.Pick > 1 {
					description = fmt.Sprintf("Time remaining: %d seconds\nYou must submit %d cards\n To do this submit your cards in order of play: 1 2 or 1,2", int(time.Until(c.GameState.NextTransition).Seconds()), c.GameState.CurrentBlackCard.Pick)
				}

				messageTitle := "Waiting for players to submit cards"

				if c.GameState.CurrentState == WaitingForCzarToPick {
					messageTitle = "Waiting for the czar to pick a winner"
					description = fmt.Sprintf("Time remaining: %d seconds", int(time.Until(c.GameState.NextTransition).Seconds()))
				}

				embed := discordgo.MessageEmbed{
					Title:       messageTitle,
					Description: description,
					Color:       0x00ff00,
				}

				// Only update every 2 seconds
				if int(time.Until(c.GameState.NextTransition).Seconds())%2 == 0 {
					_, err := s.ChannelMessageEditEmbed(
						channelID,
						c.GameState.UpdateWindowId,
						&embed,
					)

					if err != nil {
						log.Printf("Transition Timer (countdown timer) \"%s\": Error sending message: %v", messageTitle, err)
						if c.GameState.CurrentState == WaitingForCzarToPick {
							// Set the game state to start next round
							c.GameState.CurrentState = RoundStarting
							// Reset the time ticker
						} else {
							c.GameState.CurrentState = WaitingForCzarToPick
						}
					}
				}
			} else {
				err := s.ChannelMessageDelete(channelID, c.GameState.UpdateWindowId)
				if err != nil {
					log.Printf("Transition Timer (delete message): Error sending message: %v", err)
				}
				log.Printf("Transition timer is expired: %s", c.GameStatusToEnglish())
			}
			switch c.GameState.CurrentState {
			case GameStarting:
				// Game is starting
				// Do nothing
				continue
			case RoundStarting:
				// Round is starting
				// Do Nothing
				continue
			case WaitingForSubmissions:
				// Waiting for players to submit their cards
				// Check if all players have submitted a card

				currentTime := time.Now()
				// Check if the timer has expired

				if !currentTime.After(c.GameState.NextTransition) {
					continue
				}

				err := s.ChannelMessageDelete(channelID, c.GameState.UpdateWindowId)

				if err != nil {
					log.Printf("Deleting Window: Error sending message: %v", err)
				}

				var notReadyPlayers []Player

				// Is this the czar trying to cheat?
				if len(c.GameState.Players) <= 2 {
					// Set the game state to cancelled
					c.GameState.CurrentState = Cancelled
					continue
				}

				for _, player := range c.GameState.Players {
					if c.GameState.PlayersFinished[player.DiscordID] {
						continue
					}

					for _, playerCards := range c.GameState.SubmittedCardsWhite {
						if playerCards.Player.DiscordID == player.DiscordID {
							if playerCards.PlayerReady || len(playerCards.Card) == c.GameState.CurrentBlackCard.Pick {
								continue
							}
						}
					}

					if player.DiscordID == c.GameState.CurrentCzar.DiscordID {
						continue
					}

					notReadyPlayers = append(notReadyPlayers, player)
				}

				if len(notReadyPlayers) > 0 {
					// All players have not submitted a card and we timed out
					embed := discordgo.MessageEmbed{
						Title:       "Card Selection Timed Out!\n",
						Color:       0xff0000,
						Description: "The following players did not submit a card:\n",
					}

					for _, player := range notReadyPlayers {
						embed.Description += fmt.Sprintf("<@%s>\n", player.DiscordID)
					}

					_, err := s.ChannelMessageSendEmbed(channelID, &embed)
					if err != nil {
						log.Printf("Timed Out Players: Error sending message: %v", err)
					}

					if len(c.GameState.SubmittedCardsWhite) == 0 {
						// No one submitted a card, skip this round
						// Set the game state to start next round
						c.GameState.CurrentState = RoundStarting
						// Reset the time ticker
						continue
					}

					// Set the game state to waiting for the czar to pick a winner
					c.GameState.CurrentState = ShowCzarSelections

				}
				continue
			case ShowCzarSelections:
				// Do nothing
				continue
			case WaitingForCzarToPick:
				// Waiting for the czar to pick a winner
				// If the czar doesn't pick, skip this round.

				currentTime := time.Now()
				// Check if the timer has expired

				if !currentTime.After(c.GameState.NextTransition) {
					continue
				}

				err := s.ChannelMessageDelete(channelID, c.GameState.UpdateWindowId)

				if err != nil {
					log.Printf("Error sending message: %v", err)
				}

				embed := discordgo.MessageEmbed{
					Title:       "Czar Selection Timed Out!\n",
					Color:       0xff0000,
					Description: "The czar did not select a winner in time.",
				}

				_, err = s.ChannelMessageSendEmbed(channelID, &embed)
				if err != nil {
					log.Printf("Czar Timeout: Error sending message: %v", err)
				}

				// Set the game state to start next round
				c.GameState.CurrentState = RoundStarting
				// Reset the time ticker
				continue
			case DeclareGameWinner:
				// Do nothing
				currentTimer.Stop()
				return
			case GameOver:
				// Do nothing
				c.ResetGame()
				currentTimer.Stop()
				return
			case Cancelled:
				// Game was cancelled
				c.ResetGame()
				currentTimer.Stop()
				return
			}
		}
	}()

	for {
		// Check the game state
		switch c.GameState.CurrentState {
		case Cancelled:
			// Game was cancelled
			currentTimer.Stop()
			c.ResetGame()
			return
		case GameStarting:
			// Game is starting
			// Shuffle the cards
			c.ShuffleAllBlackCards()
			c.ShuffleAllWhiteCards()
			c.ShufflePlayers()
			// Deal the cards
			c.DealCardsToPlayers()
			// Set the game state to waiting for the czar
			c.GameState.CurrentState = RoundStarting
			continue
		case RoundStarting:
			c.GameState.CurrentRound++
			// Are there players in the lobby?
			if len(c.GameState.PlayersInLobby) > 0 {
				// Move players from the lobby to the game
				c.GameState.Players = append(c.GameState.Players, c.GameState.PlayersInLobby...)
				c.GameState.PlayersInLobby = nil
			}
			c.SelectCzar()
			// Round is starting
			c.ShowBlackCard(s, channelID, true)
			// Clear the buffer
			c.GameState.DiscordInput = nil

			// Reset the time ticker
			c.GameState.ReadyMessage = ""
			c.GameState.ReadyMessageID = ""
			c.GameState.SubmittedCardsWhite = nil
			transitionSeconds := 60

			for i := 0; i < c.GameState.CurrentBlackCard.Pick; i++ {
				transitionSeconds += int(60 / (float64(i) + .5))
			}

			c.GameState.NextTransition = time.Now().Add(time.Duration(transitionSeconds) * time.Second)

			description := fmt.Sprintf("Time remaining: %d seconds", int(time.Until(c.GameState.NextTransition).Seconds()))

			if c.GameState.CurrentBlackCard.Pick > 1 {
				description = fmt.Sprintf("Time remaining: %d seconds\nYou must submit %d cards\n To do this submit your cards in order of play: 1 2 or 1,2", int(time.Until(c.GameState.NextTransition).Seconds()), c.GameState.CurrentBlackCard.Pick)
			}
			// Draw the timeout window
			embed := discordgo.MessageEmbed{
				Title:       "Waiting for players to submit cards",
				Description: description,
				Color:       0x00ff00,
			}
			message, err := s.ChannelMessageSendEmbed(channelID, &embed)
			if err != nil {
				log.Printf("Card Submission: Error sending message: %v", err)
			}
			c.GameState.UpdateWindowId = message.ID
			c.GameState.CurrentState = WaitingForSubmissions
		case WaitingForSubmissions:
			// Waiting for players to submit their cards
			// Look for current players to select a card by number by looking at the buffer, delete the player's message after they select
			// If the player doesn't select a card after 1 minute, skip them
			// If all players have submitted a card, move to the next state

			// Check the buffer for messages
			input := c.GetNextInput()

			if input.Message == "" {
				continue
			}

			if !messageIsNumber(input.Message) {
				continue
			}

			// Delete the selection from the channel
			err := s.ChannelMessageDelete(input.ChannelID, input.MessageID)
			if err != nil {
				log.Printf("Error deleting message: %v", err)
			}

			// Is this the czar trying to cheat?
			if input.UserID == c.GameState.CurrentCzar.DiscordID {
				_, err := s.ChannelMessageSend(
					input.ChannelID,
					fmt.Sprintf("<@%s>, you are the czar! You cannot submit a card!\nhttps://tenor.com/view/right-to-jail-jail-parks-and-rec-right-away-fred-armisen-gif-16902115",
						input.UserID),
				)
				if err != nil {
					log.Printf("Error sending message: %v", err)
				}
				continue
			}

			var cardNumbers []string

			if strings.Contains(input.Message, ",") {
				cardNumbers = strings.Split(input.Message, ",")
			} else if strings.Contains(input.Message, " ") {
				cardNumbers = strings.Split(input.Message, " ")
			} else {
				cardNumbers = []string{input.Message}
			}

			// Did the user submit two of the same card? I.E. 1,1 or 2,2
			if len(cardNumbers) > 1 {
				if cardNumbers[0] == cardNumbers[1] {
					embed := discordgo.MessageEmbed{
						Title:       "You cannot submit the same card more than once!",
						Description: fmt.Sprintf("<@%s>, you cannot submit the same more than once!\nhttps://tenor.com/view/right-to-jail-jail-parks-and-rec-right-away-fred-armisen-gif-16902115", input.UserID),
						Color:       0xff0000,
						URL:         "https://tenor.com/view/right-to-jail-jail-parks-and-rec-right-away-fred-armisen-gif-16902115",
					}
					_, err := s.ChannelMessageSendEmbed(input.ChannelID, &embed)

					if err != nil {
						log.Printf("Error sending message: %v", err)
					}
					continue
				}
			} else if len(cardNumbers) > 2 {
				if cardNumbers[0] == cardNumbers[2] || cardNumbers[1] == cardNumbers[2] {
					embed := discordgo.MessageEmbed{
						Title:       "You cannot submit the same card more than once!",
						Description: fmt.Sprintf("<@%s>, you cannot submit the same more than once!\nhttps://tenor.com/view/right-to-jail-jail-parks-and-rec-right-away-fred-armisen-gif-16902115", input.UserID),
						Color:       0xff0000,
						URL:         "https://tenor.com/view/right-to-jail-jail-parks-and-rec-right-away-fred-armisen-gif-16902115",
					}
					_, err := s.ChannelMessageSendEmbed(input.ChannelID, &embed)
					if err != nil {
						log.Printf("Error sending message: %v", err)
					}
				}
			}

			// Check if the user submitted #, #
			for i, cardNumber := range cardNumbers {
				cardNumber = strings.TrimSpace(cardNumber)
				cardNumberInt, err := strconv.Atoi(cardNumber)

				if err != nil {
					log.Printf("Error converting string to int: %v", err)
					continue
				}

				if i < c.GameState.CurrentBlackCard.Pick {
					c.HandleUserSubmission(cardNumberInt, input, s)
				}
			}

			_ = c.UpdatePlayerSubmissionStatus(channelID, s)

		case ShowCzarSelections:
			c.ShowBlackCard(s, channelID, false)
			// Shuffle the submitted cards
			rand.Shuffle(len(c.GameState.SubmittedCardsWhite), func(i, j int) {
				c.GameState.SubmittedCardsWhite[i], c.GameState.SubmittedCardsWhite[j] = c.GameState.SubmittedCardsWhite[j], c.GameState.SubmittedCardsWhite[i]
			})
			embed := discordgo.MessageEmbed{
				Title:       fmt.Sprintf("\"%s\"\nSelect the winner:", c.GameState.CurrentBlackCard.Text),
				Description: fmt.Sprintf("<@%s>", c.GameState.CurrentCzar.DiscordID) + ", you are the czar!\nSelect the winning card by number\n",
			}
			for i, card := range c.GameState.SubmittedCardsWhite {
				embedString := fmt.Sprintf("%d. ", i+1)

				for j, whiteCard := range card.Card {
					if j == 0 {
						embedString += " " + whiteCard.Text
					} else {
						embedString += ", " + whiteCard.Text
					}

					c.RemoveCardFromPlayerHand(card.Player, whiteCard)
				}
				embedString += "\n"
				embed.Description += embedString
			}
			messageSend := &discordgo.MessageSend{
				Embed: &embed,
			}
			_, err := s.ChannelMessageSendComplex(channelID, messageSend)
			if err != nil {
				log.Printf("Error sending message: %v", err)
			}

			transitionTime := 60.0

			for _, card := range c.GameState.SubmittedCardsWhite {
				transitionTime += float64(60) / (float64(len(card.Card)) + .5)
			}

			c.GameState.NextTransition = time.Now().Add(time.Duration(transitionTime) * time.Second)

			embed = discordgo.MessageEmbed{
				Title:       "Waiting for the czar to pick a winner",
				Description: fmt.Sprintf("Time remaining: %d seconds", int(time.Until(c.GameState.NextTransition).Seconds())),
				Color:       0x00ff00,
			}

			message, err := s.ChannelMessageSendEmbed(channelID, &embed)
			if err != nil {
				log.Printf("Error sending message: %v", err)
			}

			c.GameState.UpdateWindowId = message.ID
			c.GameState.CurrentState = WaitingForCzarToPick
		case WaitingForCzarToPick:
			// Waiting for the czar to pick a winner
			input := c.GetNextInput()
			if input.Message == "" {
				continue
			}

			if input.UserID == c.GameState.CurrentCzar.DiscordID {
				if messageIsNumber(input.Message) {
					cardNumber, err := strconv.Atoi(input.Message)

					if err != nil {
						log.Printf("Error converting string to int: %v", err)
						continue
					}

					// Is the submission more or less than the cards presented?
					if cardNumber > len(c.GameState.SubmittedCardsWhite) || cardNumber < 1 {
						_, err := s.ChannelMessageSend(input.ChannelID, "That is not a valid selection!")
						if err != nil {
							log.Printf("Error sending message: %v", err)
						}
						continue
					}
					c.HandleCzarSubmission(cardNumber, input, s)

				}

			}
		case DeclareGameWinner:

			// Declare the winner of the game
			var winner Player

			// Copy the players slice
			players := make([]Player, len(c.GameState.Players))

			copy(players, c.GameState.Players)

			// Sort the players by score
			for i := range players {
				for j := range players {
					if players[i].Score > players[j].Score {
						players[i], players[j] = players[j], players[i]
					}
				}
			}

			winner = players[0]

			// Get the user's profile picture
			user, err := s.User(winner.DiscordID)

			if err != nil {
				log.Printf("Error getting user: %v", err)
			}

			embed := discordgo.MessageEmbed{
				Title:       "Game Over!",
				Description: fmt.Sprintf("<@%s> has won the game!", winner.DiscordID),
				Thumbnail: &discordgo.MessageEmbedThumbnail{
					URL: user.AvatarURL(""),
				},
				Color: 0x00ff00,
			}
			_, err = s.ChannelMessageSendEmbed(channelID, &embed)
			if err != nil {
				log.Printf("Error sending message: %v", err)
			}

			_, err = s.ChannelMessageSend(channelID, fmt.Sprintf("The winner is <@%s>!", winner.DiscordID))

			if err != nil {
				log.Printf("Error sending message: %v", err)
			}

			// Show scores
			embed = discordgo.MessageEmbed{
				Title:       "Final Scores",
				Description: "",
				Color:       0x00ff00,
			}

			for _, player := range c.GameState.Players {
				if player.DiscordID == winner.DiscordID {
					err := c.UpdatePlayerScores(player, 1, player.Score)
					if err != nil {
						log.Printf("Error updating player scores: %v", err)
					}
				} else {
					err := c.UpdatePlayerScores(player, 0, player.Score)
					if err != nil {
						log.Printf("Error updating player scores: %v", err)
					}
				}
				embed.Description += fmt.Sprintf("<@%s>: %d\n", player.DiscordID, player.Score)
			}

			_, err = s.ChannelMessageSendEmbed(channelID, &embed)

			if err != nil {
				log.Printf("Error sending message: %v", err)
			}

			// Get the scoreboard

			// Set the game state to game over
			c.GameState.CurrentState = GameOver

		case GameOver:
			// Game is over
			// Reset the game state
			// Stop the timer
			currentTimer.Stop()
			c.GameState = GameState{}
			_, err := s.ChannelMessageSend(channelID, "`^cah start` to begin a new game.")
			if err != nil {
				log.Printf("Error sending message: %v", err)
			}

			err = c.GenerateScoreboard(s)
			if err == nil {
				data, err := os.ReadFile(fmt.Sprintf("scoreboard-%s.png", c.Guild.ID))
				var file *discordgo.File
				if err == nil {

					// Create a new File struct for the image
					file = &discordgo.File{
						Name:   "scoreboard.png",
						Reader: bytes.NewReader(data),
					}

				}

				messageSend := &discordgo.MessageSend{
					Content: "The current scoreboard:",
					Files: []*discordgo.File{
						file,
					},
				}

				_, err = s.ChannelMessageSendComplex(channelID, messageSend)
				if err != nil {
					log.Printf("Error sending message: %v", err)
				}
			} else {
				log.Printf("Error generating scoreboard: %v", err)
			}

			// Stop the game loop
			c.ResetGame()
			return
		}
	}
}

func (c *CardsAgainst) ShowWinningWhiteCard(s *discordgo.Session, channelID string, winningCard SubmittedCard) {
	// Show the winning card
	// Get the user's profile picture
	user, err := s.User(winningCard.Player.DiscordID)
	if err != nil {
		log.Printf("Error getting user: %v", err)
	}

	ordinals := []string{"first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth"}

	for index, card := range winningCard.Card {
		message := "The winning card is:"

		if len(winningCard.Card) > 1 {
			message = fmt.Sprintf("The %s winning card is:", ordinals[index])
		}

		_ = c.CreateWhiteCardImage(card, channelID)

		// Load the image file
		data, err := os.ReadFile(fmt.Sprintf("whitecard-%s.png", channelID))

		var file *discordgo.File

		if err == nil {
			// Create a new File struct for the image
			file = &discordgo.File{
				Name:   "whitecard.png",
				Reader: bytes.NewReader(data),
			}

		}
		embed := discordgo.MessageEmbed{
			Title:       message,
			Description: card.Text,
			Thumbnail: &discordgo.MessageEmbedThumbnail{
				URL: user.AvatarURL("128"),
			},
		}

		messageSend := &discordgo.MessageSend{
			Content: message,
			Embed:   &embed,
		}

		if file != nil {
			messageSend.Files = []*discordgo.File{
				file,
			}
		}

		_, err = s.ChannelMessageSendComplex(channelID, messageSend)

		if err != nil {
			log.Printf("Error sending message: %v", err)
		}

		time.Sleep(2 * time.Second)

	}

}

func (c *CardsAgainst) ShowBlackCard(s *discordgo.Session, channelID string, newCard bool) {
	// Show the black card
	actionRow := discordgo.ActionsRow{}
	if newCard {
		c.GameState.CurrentBlackCard = c.GameState.ShuffledCardsBlack[0]
		// Remove the black card from the deck
		c.GameState.ShuffledCardsBlack = c.GameState.ShuffledCardsBlack[1:]
		// Use InteractionCreate to add a button to the black card so we can show the user their cards.
		button := discordgo.Button{
			Label:    "Show Cards",
			Style:    discordgo.SuccessButton,
			CustomID: "show_cards",
			Emoji:    discordgo.ComponentEmoji{Name: "🃏"},
		}

		actionRow = discordgo.ActionsRow{
			Components: []discordgo.MessageComponent{
				button,
			},
		}
	}

	cardText := c.GameState.CurrentBlackCard.Text
	// Convert _ to long underlines
	cardText = strings.ReplaceAll(cardText, "_", "＿＿＿＿")

	// Replace the card text with the current user name
	discordUser, err := s.User(c.GameState.CurrentCzar.DiscordID)
	if err != nil {
		log.Printf("Error getting user: %v", err)
	}

	member, err := s.State.Member(c.Guild.ID, discordUser.ID)

	if err != nil {
		log.Printf("(Show Black Card) Error getting member: %v", err)
	}

	var nickname string

	if member != nil {
		nickname = member.Nick
	}

	if nickname == "" {
		nickname = discordUser.Username
	}

	cardText = strings.ReplaceAll(cardText, "Insert Name", nickname)

	c.GameState.CurrentBlackCard.Text = cardText // Put the card text back here.

	imageembed := c.CreateBlackCardImage(c.GameState.CurrentBlackCard, channelID)

	embed := discordgo.MessageEmbed{
		Title:       "Black Card",
		Description: cardText,
		Image:       &imageembed,
	}

	// Load the image file
	data, err := os.ReadFile(fmt.Sprintf("blackcard-%s.png", channelID))
	var file *discordgo.File
	if err == nil {

		// Create a new File struct for the image
		file = &discordgo.File{
			Name:   "blackcard.png",
			Reader: bytes.NewReader(data),
		}

	}
	messageSend := &discordgo.MessageSend{
		Content: fmt.Sprintf("<@%s>", c.GameState.CurrentCzar.DiscordID) + ", you are the czar! The black card is:",
		Embed:   &embed,
	}

	if file != nil {
		messageSend.Files = []*discordgo.File{
			file,
		}
	}

	if actionRow.Components != nil {
		messageSend.Components = []discordgo.MessageComponent{
			actionRow,
		}
	}

	// Send the black card to the channel
	_, err = s.ChannelMessageSendComplex(channelID, messageSend)
	if err != nil {
		log.Printf("Error sending message: %v", err)
	}
}

func (c *CardsAgainst) SelectCzar() {
	// Select the czar
	c.GameState.CurrentCzar = c.GameState.Players[0]
	// Remove the czar from the players slice
	c.GameState.Players = c.GameState.Players[1:]
	// Shuffle czar to last position
	c.GameState.Players = append(c.GameState.Players, c.GameState.CurrentCzar)
}

func (c *CardsAgainst) HandleCzarSubmission(cardNumber int, input DiscordInput, s *discordgo.Session) {

	cardNumber-- // Decrement the selection by 1 to match the slice index
	winner := c.GameState.SubmittedCardsWhite[cardNumber].Player
	for i, player := range c.GameState.Players {
		if player.DiscordID == winner.DiscordID {
			c.GameState.Players[i].Score++
			if c.GameState.Players[i].Score >= c.GameState.PointsToWin {
				c.GameState.CurrentState = DeclareGameWinner

			} else {
				// Create the image embed

				embed := discordgo.MessageEmbed{
					Title:       "Round Over!",
					Description: fmt.Sprintf("<@%s> has won the round!", winner.DiscordID),
					Color:       0x00ff00,
				}

				_, err := s.ChannelMessageSendEmbed(input.ChannelID, &embed)

				if err != nil {
					log.Printf("Error sending message: %v", err)
				}

				// Get the winning card image
				c.ShowWinningWhiteCard(s, input.ChannelID, c.GameState.SubmittedCardsWhite[cardNumber])

				_, err = s.ChannelMessageSend(input.ChannelID, "The scores are:")

				if err != nil {
					log.Printf("Error sending message: %v", err)
				}

				// make a copy of the Players array
				players := make([]Player, len(c.GameState.Players))

				copy(players, c.GameState.Players)

				// Sort the players by score
				for i := range players {
					for j := range players {
						if players[i].Score > players[j].Score {
							players[i], players[j] = players[j], players[i]
						}
					}
				}
				for i, player := range players {
					user, err := s.User(player.DiscordID)
					if err != nil {
						log.Printf("Error getting user: %v", err)
					}
					// Top three should get a gold, silver and bronze color everyone else black
					medalColor := 0x000000
					switch i {
					case 0:
						medalColor = 0xffd700
					case 1:
						medalColor = 0xc0c0c0
					case 2:
						medalColor = 0xcd7f32
					}
					embed := discordgo.MessageEmbed{
						Title:       player.Name,
						Description: fmt.Sprintf("%d", player.Score),
						Color:       medalColor,
						Thumbnail: &discordgo.MessageEmbedThumbnail{
							URL: user.AvatarURL("128"),
						},
					}

					_, err = s.ChannelMessageSendEmbed(input.ChannelID, &embed)

					if err != nil {
						log.Printf("Error sending message: %v", err)
					}
				}
				c.GameState.CurrentState = RoundStarting
			}
		}
	}
}

func (c *CardsAgainst) HandleUserSubmission(selection int, input DiscordInput, s *discordgo.Session) {
	if c.GameState.PlayersFinished[input.UserID] {
		return
	}
	// Check if the message is a number and is in the player's hand
	selection-- // Decrement the selection by 1 to match the slice index

	if selection < 0 || selection >= 10 {
		_, err := s.ChannelMessageSend(input.ChannelID, "You do not have that card! https://tenor.com/view/right-to-jail-jail-parks-and-rec-right-away-fred-armisen-gif-16902115")
		if err != nil {
			log.Printf("Error sending message: %v", err)
		}
		return
	}

	for _, player := range c.GameState.Players {
		if player.DiscordID == input.UserID {
			selectedCard := player.Hand[selection]

			if len(player.CardsUsed) > 0 {
				for _, card := range player.CardsUsed {
					if card.Text == selectedCard.Text {
						return
					}
				}
			}

			player.CardsUsed = append(player.CardsUsed, selectedCard)

			SawPlayer := false

			var cardsSubmitted SubmittedCard = SubmittedCard{}

			for _, playerCards := range c.GameState.SubmittedCardsWhite {
				if input.UserID == playerCards.Player.DiscordID {
					cardsSubmitted = playerCards
					SawPlayer = true
				}
			}

			// Player was not found in the submitted cards slice
			if !SawPlayer {
				c.GameState.SubmittedCardsWhite = append(c.GameState.SubmittedCardsWhite, SubmittedCard{
					Player:      player,
					Card:        []WhiteCard{player.Hand[selection]},
					PlayerReady: bool(len(cardsSubmitted.Card) == c.GameState.CurrentBlackCard.Pick),
				})

				cardsSubmitted = c.GameState.SubmittedCardsWhite[len(c.GameState.SubmittedCardsWhite)-1]
				continue
			}

			if len(cardsSubmitted.Card) == c.GameState.CurrentBlackCard.Pick {
				c.GameState.PlayersFinished[input.UserID] = true
				continue
			}

			cardsSubmitted.Card = append(cardsSubmitted.Card, player.Hand[selection])

			// Replace the player's submitted cards if they have already submitted
			for i, playerCards := range c.GameState.SubmittedCardsWhite {
				if playerCards.PlayerReady || len(playerCards.Card) == c.GameState.CurrentBlackCard.Pick {
					continue
				}
				if playerCards.Player.DiscordID == input.UserID {
					c.GameState.SubmittedCardsWhite[i] = cardsSubmitted
					if len(cardsSubmitted.Card) == c.GameState.CurrentBlackCard.Pick {
						c.GameState.SubmittedCardsWhite[i].PlayerReady = true
					}
					for j, card := range c.GameState.SubmittedCardsWhite[i].Card {
						if card.Text == "Fill in the Blank" {
							go func(i int, j int) {
								// Send the player a DM
								cardText, err := c.AskForCardContent(s, input.UserID)
								if err != nil {
									log.Printf("Error sending message: %v", err)
									return
								}
								c.GameState.SubmittedCardsWhite[i].Card[j].Text = cardText
							}(i, j)
						}
					}
				}
			}

			// Delete the message
			err := s.ChannelMessageDelete(input.ChannelID, input.MessageID)

			if err != nil {
				log.Printf("Error deleting message: %v", err)
			}

			log.Printf("Player %s submitted card %d\ncards submitted %+v", player.Name, selection, cardsSubmitted)

		}
	}
}

func (c *CardsAgainst) UpdatePlayerSubmissionStatus(channelID string, s *discordgo.Session) bool {
	c.mux.Lock()
	defer c.mux.Unlock()
	// Update the player submission status
	// Check if all players have submitted a card

	var readyPlayers []Player
	var notReadyPlayers []Player

	for _, playerCards := range c.GameState.SubmittedCardsWhite {
		if playerCards.PlayerReady || len(playerCards.Card) == c.GameState.CurrentBlackCard.Pick {
			readyPlayers = append(readyPlayers, playerCards.Player)
		}
	}

	for _, player := range c.GameState.Players {
		if !c.GameState.PlayersFinished[player.DiscordID] && player.DiscordID != c.GameState.CurrentCzar.DiscordID {
			showUnready := true
			for _, readyPlayer := range readyPlayers {
				if player.DiscordID == readyPlayer.DiscordID {
					showUnready = false
				}
			}
			if showUnready {
				notReadyPlayers = append(notReadyPlayers, player)
			}
		}
	}

	newReadyMessage := "Player Status:\n"

	for _, player := range readyPlayers {
		newReadyMessage += fmt.Sprintf("✅ <@%s>\n", player.DiscordID)
	}

	for _, player := range notReadyPlayers {
		newReadyMessage += fmt.Sprintf("❌ <@%s>\n", player.DiscordID)
	}

	embed := discordgo.MessageEmbed{
		Title:       "Player Submission Status",
		Color:       0xfff52e,
		Description: newReadyMessage,
	}

	if c.GameState.ReadyMessage != newReadyMessage || c.GameState.ReadyMessageID == "" {
		c.GameState.ReadyMessage = newReadyMessage
		if c.GameState.ReadyMessageID == "" {
			messageSend := &discordgo.MessageSend{
				Embed: &embed,
			}
			message, err := s.ChannelMessageSendComplex(channelID, messageSend)
			if err != nil {
				log.Printf("Error sending embed: %v", err)
			}
			c.GameState.ReadyMessageID = message.ID
		} else {
			_, err := s.ChannelMessageEditComplex(&discordgo.MessageEdit{
				ID:      c.GameState.ReadyMessageID,
				Channel: channelID,
				Embed:   &embed,
			})
			if err != nil {
				log.Printf("Error editing embed: %v", err)
			}
		}
	}

	if len(readyPlayers)+1 >= len(c.GameState.Players) {
		// All players have submitted a card
		// Show the cards selected with the question
		log.Printf("All players have submitted a card!")
		// Set the game state to waiting for the czar to pick a winner
		c.GameState.CurrentState = ShowCzarSelections
		// Reset the time ticker
		return true
	}
	return false
}
