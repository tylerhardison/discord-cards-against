package ask

import (
	"context"
	"fmt"
	"os"

	"github.com/sashabaranov/go-openai"
)

func AskGPT4(username, question string) string {
	// Strip new lines from api key
	key := os.Getenv("OPENAI_API_KEY")
	if len(key) > 0 && key[len(key)-1] == '\n' {
		key = key[:len(key)-1]
	}
	client := openai.NewClient(key)

	req := openai.ChatCompletionRequest{
		Model: openai.GPT4TurboPreview,
		Messages: []openai.ChatCompletionMessage{
			{
				Role:    openai.ChatMessageRoleSystem,
				Content: question,
			},
		},
		User: username,
	}

	resp, err := client.CreateChatCompletion(context.Background(), req)

	if err != nil {
		fmt.Printf("ChatCompletion error: %v\n", err)
		return ""
	}

	return resp.Choices[0].Message.Content
}
