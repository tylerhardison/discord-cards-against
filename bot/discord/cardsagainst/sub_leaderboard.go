package cardsagainst

import (
	"bytes"
	"fmt"
	"log"
	"os"

	"github.com/bwmarrin/discordgo"
)

func (c *CardsAgainst) LeaderboardCommand(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate) (string, error) {
	// Get the leaderboard for the current game
	channelID := c.GetChannelId(m, i)
	err := c.GenerateScoreboard(s)
	if err == nil {
		data, err := os.ReadFile(fmt.Sprintf("scoreboard-%s.png", c.Guild.ID))
		var file *discordgo.File
		if err == nil {

			// Create a new File struct for the image
			file = &discordgo.File{
				Name:   "scoreboard.png",
				Reader: bytes.NewReader(data),
			}

		}

		messageSend := &discordgo.MessageSend{
			Content: "The current scoreboard:",
			Files: []*discordgo.File{
				file,
			},
		}

		_, err = s.ChannelMessageSendComplex(channelID, messageSend)
		if err != nil {
			log.Printf("Error sending message: %v", err)
		}
	} else {
		_, err = c.SendErrorMessage(s, m, i, []string{"No scoreboard was found, make sure you have played at least one game."})
		if err != nil {
			log.Printf("Error sending message: %v", err)
		}
	}
	return "success", nil
}
