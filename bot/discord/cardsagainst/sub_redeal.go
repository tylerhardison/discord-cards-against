package cardsagainst

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

func (c *CardsAgainst) TradePointsForNewCards(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate) (bool, error) {

	if !c.IsInPlayingState() {
		return false, nil
	}

	var discordID string

	if m != nil {
		discordID = m.Author.ID
	} else if i != nil {
		discordID = i.Member.User.ID
	}

	if !c.PlayerInGame(Player{DiscordID: discordID}) {
		return false, nil
	}

	// Find the player, check if they have > 2 points, remove all their cards and redeal them

	log.Printf("TradePointsForNewCards: %s", discordID)

	c.mux.Lock()
	defer c.mux.Unlock()

	log.Printf("TradePointsForNewCards: Locked Mutex: %s", discordID)
	for i, p := range c.GameState.Players {
		if p.DiscordID == discordID {
			log.Printf("TradePointsForNewCards: Found player: %s", discordID)
			if p.Score < 2 {
				return false, nil
			}
			c.GameState.Players[i].Score -= 2
			c.GameState.Players[i].Hand = []WhiteCard{}
			for j := 0; j < 10; j++ {
				c.GameState.Players[i].Hand = append(c.GameState.Players[i].Hand, c.DealWhiteCard())
			}
			return true, nil
		}
	}

	return true, nil
}
