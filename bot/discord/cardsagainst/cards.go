package cardsagainst

import (
	"encoding/json"
	"fmt"
	"image"
	"image/gif"
	"log"
	"math/rand"
	"net/http"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/fogleman/gg"
	"github.com/mitchellh/go-wordwrap"
)

const (
	WhiteCardCount = 10
)

func (c *CardsAgainst) ShuffleAllBlackCards() bool {
	c.mux.Lock()
	defer c.mux.Unlock()
	// Take the selected packs and shuffle the black cards into a new slice
	// Get all the black cards from the selected packs
	var blackCards []BlackCard
	for _, pack := range c.GameState.SelectedPacks {
		for _, card := range c.Cards[pack].Black {
			cardText := strings.ReplaceAll(card.Text, "_", "＿＿＿＿")
			blackCards = append(blackCards, BlackCard{Text: cardText, Pick: card.Pick})
		}
	}

	// Shuffle the black cards
	rand.Shuffle(len(blackCards), func(i, j int) { blackCards[i], blackCards[j] = blackCards[j], blackCards[i] })

	// Set the shuffled cards to the game state
	c.GameState.ShuffledCardsBlack = blackCards
	return true
}

func (c *CardsAgainst) ShuffleAllWhiteCards() {
	c.mux.Lock()
	defer c.mux.Unlock()
	// Take the selected packs and shuffle the white cards into a new slice
	// Get all the white cards from the selected packs
	var whiteCards []WhiteCard
	seenCards := make(map[string]bool)
	for _, pack := range c.GameState.SelectedPacks {
		for _, card := range c.Cards[pack].White {
			if seenCards[card.Text] {
				// Card has already been seen
				continue
			}
			whiteCards = append(whiteCards, WhiteCard{Text: card.Text})
		}
	}
	// Shuffle the white cards
	rand.Shuffle(len(whiteCards), func(i, j int) { whiteCards[i], whiteCards[j] = whiteCards[j], whiteCards[i] })

	// Set the shuffled cards to the game state
	c.GameState.ShuffledCardsWhite = whiteCards
}

func (c *CardsAgainst) DealCardsToPlayers() bool {
	// Deal 10 cards to each player

	for i := 0; i < WhiteCardCount; i++ {
		log.Printf("Dealing card %d", i)
		for j := 0; j < len(c.GameState.Players); j++ {
			whiteCard := c.DealWhiteCard()
			log.Printf("Dealing card %+v to player %d", whiteCard, j)
			c.GameState.Players[j].Hand = append(c.GameState.Players[j].Hand, whiteCard)
		}
	}

	// Print out everyone's decks as JSON
	for _, player := range c.GameState.Players {
		// Convert hand to JSON
		cardsJson, err := json.Marshal(player.Hand)

		if err != nil {
			log.Println(err)
		}

		log.Printf("Player %s's deck: %+v", player.Name, string(cardsJson))
	}
	return true
}

func (c *CardsAgainst) CreateWhiteCardImage(card WhiteCard, channelId string) discordgo.MessageEmbedImage {
	var err error
	card.Text = strings.ReplaceAll(card.Text, "＿", "_")
	const W = 640
	const H = 886
	const Margin = 40 // Added margin constant

	dc := gg.NewContext(W, H)
	dc.SetRGB(1, 1, 1)
	dc.DrawRectangle(0, 0, float64(W), float64(H))
	dc.Fill()

	var S = float64(64) // Font size

	// String length too long? Decrease font size
	if len(card.Text) > 100 {
		S = float64(48)
	}
	err = dc.LoadFontFace("../fonts/DejaVuSans-Bold.ttf", S)
	if err != nil {
		log.Println(err)
	}

	dc.SetRGB(0, 0, 0)

	// Wrap the text to fit the width
	wrapWidth := uint(W - 2*Margin) // Calculate the wrap width considering the margins
	wrappedText := wordwrap.WrapString(card.Text, wrapWidth)

	// Draw the wrapped text starting at the upper left corner with a 25px margin
	dc.DrawStringWrapped(
		wrappedText,
		float64(Margin),
		float64(Margin),
		float64(0),
		float64(0),
		float64(W-2*Margin),
		1.5,
		gg.AlignLeft,
	)

	// Draw the Guild Icon at the lower left of the card
	resp, err := http.Get(c.Guild.IconURL("64"))
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	img, _, err := image.Decode(resp.Body)
	if err != nil {
		img, err = gif.Decode(resp.Body)
		if err != nil {
			log.Printf("Error decoding image: %v %s", err, c.Guild.IconURL("64"))
		}
	}

	// Only draw the image if it was successfully decoded
	if img != nil {
		dc.DrawImage(img, Margin, H-Margin-img.Bounds().Dy())
	}

	// Draw the Guild Name as the lower left text
	err = dc.LoadFontFace("../fonts/DejaVuSans.ttf", 24)

	if err != nil {
		log.Println(err)
	}

	dc.SetRGB(0, 0, 0)
	// String should be left aligned
	dc.DrawStringAnchored(fmt.Sprintf("Cards Against %s", c.Guild.Name), float64(Margin+img.Bounds().Dx()+8), float64(H-Margin-32), 0, 1)

	err = dc.SavePNG(fmt.Sprintf("whitecard-%s.png", channelId))
	if err != nil {
		log.Println(err)
	}

	return discordgo.MessageEmbedImage{
		URL: "attachment://whitecard.png",
	}
}

func (c *CardsAgainst) CreateBlackCardImage(card BlackCard, channelId string) discordgo.MessageEmbedImage {
	var err error
	card.Text = strings.ReplaceAll(card.Text, "＿", "_")
	const W = 640
	const H = 886
	const Margin = 40 // Added margin constant

	dc := gg.NewContext(W, H)
	dc.SetRGB(0, 0, 0)
	dc.DrawRectangle(0, 0, float64(W), float64(H))
	dc.Fill()

	var S = float64(64) // Font size

	// String length too long? Decrease font size
	if len(card.Text) > 100 {
		S = float64(48)
	}
	err = dc.LoadFontFace("../fonts/DejaVuSans-Bold.ttf", S)
	if err != nil {
		log.Println(err)
	}

	dc.SetRGB(1, 1, 1)

	// Wrap the text to fit the width
	wrapWidth := uint(W - 2*Margin) // Calculate the wrap width considering the margins
	wrappedText := wordwrap.WrapString(card.Text, wrapWidth)

	// Draw the wrapped text starting at the upper left corner with a 25px margin
	dc.DrawStringWrapped(
		wrappedText,
		float64(Margin),
		float64(Margin),
		float64(0),
		float64(0),
		float64(W-2*Margin),
		1.5,
		gg.AlignLeft,
	)

	// Draw the Guild Icon at the lower left of the card
	resp, err := http.Get(c.Guild.IconURL("64"))
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	img, _, err := image.Decode(resp.Body)
	if err != nil {
		img, err = gif.Decode(resp.Body)
		if err != nil {
			log.Printf("Error decoding image: %v %s", err, c.Guild.IconURL("64"))
		}
	}

	// Only draw the image if it was successfully decoded
	if img != nil {
		dc.DrawImage(img, Margin, H-Margin-img.Bounds().Dy())
	}

	// Draw the "Pick %d" text
	if card.Pick > 1 {
		err = dc.LoadFontFace("../fonts/DejaVuSans.ttf", 32)
		if err != nil {
			log.Println(err)
		}

		dc.SetRGB(1, 1, 1)
		// String should be right aligned
		dc.DrawStringAnchored("Pick", float64(W-Margin-16), float64(H-Margin-32), 1, 1)
		// Draw a white circle where the number will go
		dc.SetRGB(1, 1, 1)
		dc.DrawCircle(float64(W-Margin-32), float64(H-Margin-32), float64(32))
		dc.Fill()
		dc.SetRGB(0, 0, 0)
		dc.DrawStringAnchored(fmt.Sprintf("%d", card.Pick), float64(W-Margin-32), float64(H-Margin-32), 0.5, 0.5)

	}

	// Draw the Guild Name as the lower left text
	err = dc.LoadFontFace("../fonts/DejaVuSans.ttf", 24)

	if err != nil {
		log.Println(err)
	}

	dc.SetRGB(1, 1, 1)
	// String should be left aligned
	dc.DrawStringAnchored(fmt.Sprintf("Cards Against %s", c.Guild.Name), float64(Margin+img.Bounds().Dx()+8), float64(H-Margin-32), 0, 1)

	err = dc.SavePNG(fmt.Sprintf("blackcard-%s.png", channelId))
	if err != nil {
		log.Println(err)
	}

	return discordgo.MessageEmbedImage{
		URL: "attachment://blackcard.png",
	}
}

func (c *CardsAgainst) DealCardsToPlayer(player Player) bool {

	// Deal 10 cards to the player
	for i := 0; i < WhiteCardCount; i++ {
		player.Hand = append(player.Hand, c.DealWhiteCard())
	}

	return true
}

func (c *CardsAgainst) DealWhiteCard() WhiteCard {
	// We have a deck of shuffled cards
	// We need to take the top card and deal it to the player
	// Then we need to remove that card from the deck
	// Then we need to return the card to the player
	// Check that we haven't already dealt this card
	for _, card := range c.GameState.BurnedCards {
		if card.Text == c.GameState.ShuffledCardsWhite[0].Text {
			// Card has already been dealt
			c.GameState.ShuffledCardsWhite = c.GameState.ShuffledCardsWhite[1:] // Remove the card from the slice
			return c.DealWhiteCard()
		}
	}
	card := c.GameState.ShuffledCardsWhite[0]
	c.GameState.ShuffledCardsWhite = c.GameState.ShuffledCardsWhite[1:]
	c.GameState.BurnedCards = append(c.GameState.BurnedCards, card)
	return card
}

func (c *CardsAgainst) AskForCardContent(s *discordgo.Session, userID string) (string, error) {
	// Create a DM channel with the user
	channel, err := s.UserChannelCreate(userID)
	if err != nil {
		return "", err
	}

	// Send a message asking for the card content
	msg, err := s.ChannelMessageSend(channel.ID, "Please enter the content for your blank card:")
	if err != nil {
		return "", err
	}

	// Wait for the user's response
	for {
		response, err := s.ChannelMessage(channel.ID, msg.ID)
		if err != nil {
			return "", err
		}

		// Check if the response is from the same user
		if response.Author.ID == userID {
			// Return the content of the response
			return response.Content, nil
		}
	}
}


func (c *CardsAgainst) RemoveCardFromPlayerHand(player Player, card WhiteCard) {
	// Remove the card from the player's hand
	for i, playerCard := range player.Hand {
		if playerCard.Text == card.Text {
			player.Hand = append(player.Hand[:i], player.Hand[i+1:]...)
			player.Hand = append(player.Hand, c.DealWhiteCard())
			return
		}
	}
}
