package cardsagainst

import (
	"fmt"
	"math/rand"
)

// Handles the player's presence
func (c *CardsAgainst) AddNewPlayer(player Player) (message string) {
	// Double check that the player isn't already in the game
	if c.PlayerInGame(player) {
		return "You are already in the game!"
	}
	c.GameState.Players = append(c.GameState.Players, player)
	return fmt.Sprintf("<@%s> has joined the game!", player.DiscordID)
}

func (c *CardsAgainst) PlayerInGame(player Player) bool {
	if len(c.GameState.Players) == 0 {
		return false
	}
	for _, p := range c.GameState.Players {
		if p.DiscordID == player.DiscordID {
			return true
		}
	}
	return false
}
func (c *CardsAgainst) RemovePlayer(player Player) (message string) {
	for i, p := range c.GameState.Players {
		if p.DiscordID == player.DiscordID {
			c.GameState.Players = append(c.GameState.Players[:i], c.GameState.Players[i+1:]...)
			return fmt.Sprintf("<@%s> has left the game!", player.DiscordID)
		}
	}
	return "You are not in the game!"
}

func (c *CardsAgainst) GetPlayerNames() []string {
	var names []string
	for _, player := range c.GameState.Players {
		names = append(names, player.Name)
	}
	return names
}

func (c *CardsAgainst) MentionPlayerNames() []string {
	var names []string
	for _, player := range c.GameState.Players {
		names = append(names, "<@"+player.DiscordID+">")
	}
	return names
}

func (c *CardsAgainst) ShufflePlayers() {
	// Shuffle the players
	for i := range c.GameState.Players {
		j := rand.Intn(i + 1)
		c.GameState.Players[i], c.GameState.Players[j] = c.GameState.Players[j], c.GameState.Players[i]
	}

}

func (c *CardsAgainst) GetPlayerByDiscordID(discordID string) Player {
	for _, player := range c.GameState.Players {
		if player.DiscordID == discordID {
			return player
		}
	}
	// Are they in the lobby?
	for _, player := range c.GameState.PlayersInLobby {
		if player.DiscordID == discordID {
			return player
		}
	}
	return Player{}
}

func (c *CardsAgainst) KickPlayer(playerId string) {
	for i, player := range c.GameState.Players {
		if player.DiscordID == playerId {
			c.GameState.Players = append(c.GameState.Players[:i], c.GameState.Players[i+1:]...)
			return
		}
	}
}
