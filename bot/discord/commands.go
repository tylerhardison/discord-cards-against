package discord

import (
	"github.com/bwmarrin/discordgo"
)

type CommandFunction func(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error

type Command struct {
	Name        string
	Description string
	Usage       string
	Example     string
	Aliases     []string
	Function    CommandFunction
	Options     []*discordgo.ApplicationCommandOption
}

func (b *Bot) InitializeCommands(s *discordgo.Session) ([]Command, []*discordgo.ApplicationCommand) {
	commands := b.GetCommands()

	var applicationCommands []*discordgo.ApplicationCommand
	for _, c := range commands {
		applicationCommands = append(applicationCommands, &discordgo.ApplicationCommand{
			Name:        c.Name,
			Description: c.Description,
			Options:     c.Options,
		})
	}

	return commands, applicationCommands
}

func (b *Bot) GetCommands() []Command {
	return []Command{
		{
			Name:        "ask",
			Description: "Ask a question.",
			Usage:       "ask <question>",
			Example:     "ask What is the meaning of life?",
			Aliases:     []string{"a"},
			Function:    b.Ask,
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "question",
					Description: "The question to ask.",
					Required:    true,
				},
			},
		},
		{
			Name:        "about",
			Description: "Gets information about the bot.",
			Usage:       "about",
			Example:     "about",
			Aliases:     []string{"info"},
			Function:    b.GetAboutInformation,
			Options:     []*discordgo.ApplicationCommandOption{},
		},
		{
			Name:        "ping",
			Description: "Pings the bot.",
			Usage:       "ping",
			Example:     "ping",
			Aliases:     []string{},
			Function:    b.Ping,
			Options:     []*discordgo.ApplicationCommandOption{},
		},
		{
			Name:        "weather",
			Description: "Gets the weather for a location.",
			Usage:       "weather <location>",
			Example:     "weather 90210",
			Aliases:     []string{"w"},
			Function:    b.Weather,
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "location",
					Description: "The location to get the weather for.",
					Required:    true,
				},
			},
		},
		{
			Name:        "devito",
			Description: "Gets a random Danny Devito quote.",
			Usage:       "devito",
			Example:     "devito",
			Aliases:     []string{"danny"},
			Function:    b.Devito,
			Options:     []*discordgo.ApplicationCommandOption{},
		},
		{
			Name:        "selftest",
			Description: "Tests the bot's connection to the database.",
			Usage:       "selftest",
			Example:     "selftest",
			Aliases:     []string{"st"},
			Function:    b.SelfTest,
			Options:     []*discordgo.ApplicationCommandOption{},
		},
		{
			Name:        "help",
			Description: "Gets general help or help for a specific command.",
			Usage:       "help [command]",
			Example:     "help weather",
			Aliases:     []string{"h"},
			Function:    b.Help,
			Options:     []*discordgo.ApplicationCommandOption{},
		},
		{
			Name:        "username",
			Description: "Generate a series of usernames using GPT4 by giving a prompt.",
			Usage:       "username <prompt>",
			Example:     "username <prompt>",
			Aliases:     []string{"usernames"},
			Function:    b.Username,
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "prompt",
					Description: "The prompt to use to generate usernames.",
					Required:    true,
				},
			},
		},
		{
			Name:        "preferences",
			Description: "Gets or sets server preferences. Valid subcommands are: `announce`, `startup`, `prefix`",
			Usage:       "preferences <subcommand> [arguments]",
			Example:     "preferences announce #general",
			Aliases:     []string{"prefs"},
			Function:    b.HandleServerPreferences,
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "announce",
					Description: "Sets the channel to announce events in.",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionChannel,
							Name:        "channel",
							Description: "The channel to announce events in.",
							Required:    false,
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "startup",
					Description: "Turns on or off the startup announcement.",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionBoolean,
							Name:        "on",
							Description: "Turns on the startup announcement.",
							Required:    false,
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "prefix",
					Description: "Sets the prefix for the bot.",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionString,
							Name:        "prefix",
							Description: "The prefix to set the bot to.",
							Required:    false,
						},
					},
				},
			},
		},
		{
			Name:        "cah",
			Description: "Starts a game of Cards Against Humanity.",
			Usage:       "cah",
			Example:     "cah",
			Aliases:     []string{},
			Function:    b.HumansAgainstPlasticRectangles,
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "leaderboard",
					Description: "Gets the leaderboard for Cards Against Humanity.",
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "selftest",
					Description: "Tests the bot's ability to play CAH against itself.",
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "start",
					Description: "Starts a game of Cards Against Humanity.",
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "join",
					Description: "Joins a game of Cards Against Humanity.",
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "leave",
					Description: "Leaves a game of Cards Against Humanity.",
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "end",
					Description: "Ends a game of Cards Against Humanity.",
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "status",
					Description: "Gets the status of a game of Cards Against Humanity.",
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "reset",
					Description: "Resets a game of Cards Against Humanity.",
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "kick",
					Description: "Kicks a player from a game of Cards Against Humanity.",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionUser,
							Name:        "user",
							Description: "The user to kick from the game.",
							Required:    true,
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "points",
					Description: "Gets or Sets the points for a game of Cards Against Humanity.",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionInteger,
							Name:        "points",
							Description: "The number of points to set the game to.",
							Required:    false,
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "decks",
					Description: "Gets or Sets the decks for a game of Cards Against Humanity.",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionString,
							Name:        "decks",
							Description: "The decks to set the game to.",
							Required:    false,
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "add",
					Description: "Adds a player to a game of Cards Against Humanity.",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionUser,
							Name:        "user",
							Description: "The user to add to the game.",
							Required:    true,
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "trade",
					Description: "Trade 2 points for a redeal.",
				},
			},
		},
		{
			Name:        "horoscope",
			Description: "Gets a horoscope for any phrase that you choose.",
			Usage:       "horoscope <phrase>",
			Example:     "horoscope <phrase>",
			Aliases:     []string{"horo"},
			Function:    b.Horoscope,
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "phrase",
					Description: "The phrase to get a horoscope for.",
					Required:    false,
				},
			},
		},
	}

}
