package discord

import (
	"log"
	"os"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/mchineboy/cahbot/bot/discord/cardsagainst"
	"go.mongodb.org/mongo-driver/mongo"
)

var games = make(map[string]*cardsagainst.CardsAgainst)

type Bot struct {
	Session         *discordgo.Session
	MongoClient     *mongo.Client
	Commands        []Command
	ActivationSigil string
	GuildSigils     map[string]string
	GuildCommands   []*discordgo.ApplicationCommand
}

func NewBot(session *discordgo.Session, mongoClient *mongo.Client) *Bot {
	b := &Bot{
		Session:     session,
		MongoClient: mongoClient,
	}

	b.Commands, b.GuildCommands = b.InitializeCommands(session)

	b.ActivationSigil = os.Getenv("ACTIVATION_SIGIL")

	return b
}

func (b *Bot) CommandsHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.Bot {
		return
	}

	if games[m.ChannelID] != nil {
		games[m.ChannelID].UpdateInputFromDiscord(m.GuildID, m.ChannelID, m.Content, m.Author.ID, m.ID)
	}

	// Check the GuildSigils map for the guild's activation sigil
	// If it's not there, use the default
	if b.GuildSigils[m.GuildID] == "" {
		b.GuildSigils[m.GuildID] = b.ActivationSigil
	}

	if !strings.HasPrefix(m.Content, b.GuildSigils[m.GuildID]) {
		return
	}

	components := strings.Split(m.Content, " ")
	command := components[0][1:]
	arguments := components[1:]

	log.Printf("Command: %s\nArguments: %s", command, arguments)
	// Scan b.Commands for the command
	for _, c := range b.Commands {
		if c.Name == command {
			// We found the command, run it
			err := c.Function(s, m, nil, arguments)
			if err != nil {
				log.Printf("Error running command %s: %s", c.Name, err.Error())
			}
			return
		}
	}

}

func (b *Bot) GetChannelId(m *discordgo.MessageCreate, i *discordgo.InteractionCreate) string {
	if m != nil {
		return m.ChannelID
	} else if i != nil {
		return i.ChannelID
	}
	return ""
}
