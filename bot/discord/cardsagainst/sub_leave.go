package cardsagainst

import (
	"github.com/bwmarrin/discordgo"
)

func (c *CardsAgainst) LeaveGameCommand(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate) (string, error) {
	// Join a game of Cards Against Humanity
	discordID := ""
	if m != nil {
		discordID = m.Author.ID
	}
	if i != nil {
		discordID = i.Member.User.ID
	}

	if !c.IsInPlayingState() {
		return "There is no game in progress!", nil
	}

	player := Player{DiscordID: discordID}

	if !c.PlayerInGame(player) {
		_, err := c.SendResponse(s, m, i, []string{"You are not in the game!"}, true)
		if err != nil {
			return "Error sending message", err
		}
	}

	c.RemovePlayer(player)

	_, err := c.SendResponse(s, m, i, []string{"You have left the game!"}, true)

	if err != nil {
		return "Error sending message", err
	}

	return "success", nil
}
