package cardsagainst

import (
	"log"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

func (c *CardsAgainst) StartGameCommand(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, guild *discordgo.Guild, activationSigil string) error {

	// Start a new game of Cards Against Humanity
	// We need to get the number of players and the number of points to win
	// The number of players is the first argument
	// The number of points to win is the second argument
	// If no arguments are passed, we default to 3 players and 5 points to win

	channelId := c.GetChannelId(m, i)
	authId := c.GetAuthorId(m, i)

	if c.GameState.CurrentState > GameStarting && c.GameState.CurrentState < GameOver {
		_, err := c.SendResponse(s, m, i, []string{"A game is already in progress!"}, true)
		if err != nil {
			log.Printf("Start: Error sending message: %s", err.Error())
		}
		return err
	}

	if c.GameState.CurrentState == WaitingForPlayers {
		if c.GameState.Host.DiscordID != authId {
			_, err := c.SendResponse(s, m, i, []string{"Only the host can start the game!"}, true)
			if err != nil {
				log.Printf("Start: Error sending message: %s", err.Error())
			}
			return err
		}
		// Requires a minimum of 3 players
		if len(c.GameState.Players) < 3 {
			_, err := c.SendResponse(s, m, i, []string{"You need at least 3 players to start the game!"}, true)
			if err != nil {
				log.Printf("Start: Error sending message: %s", err.Error())
			}
			return err
		}
		// Lock the game state players can only be added by host now
		_, err := c.SendResponse(s, m, i, []string{"The game is now locked. Only the host can add players.\n^Starting the game in 10 seconds..."}, false)
		if err != nil {
			log.Printf("Start: Error sending message: %s", err.Error())
		}

		c.GameState.CurrentState = GameStarting
		// Sleep for 30 seconds, start the game loop
		time.Sleep(10 * time.Second)
		go c.GameLoop(s, channelId)
		return nil
	}

	if m != nil {
		// Add the player who started the game
		_ = c.AddNewPlayer(Player{
			Name:      m.Author.Username,
			DiscordID: m.Author.ID,
			ChannelID: m.ChannelID,
		})

		c.GameState.Host = Player{
			Name:      m.Author.Username,
			DiscordID: m.Author.ID,
			ChannelID: m.ChannelID,
		}
	}
	if i != nil {
		// Add the player who started the game
		_ = c.AddNewPlayer(Player{
			Name:      i.Member.User.Username,
			DiscordID: i.Member.User.ID,
			ChannelID: i.ChannelID,
		})

		c.GameState.Host = Player{
			Name:      i.Member.User.Username,
			DiscordID: i.Member.User.ID,
			ChannelID: i.ChannelID,
		}
	}

	// Set the game state
	c.GameState.CurrentState = WaitingForPlayers

	joinButton := discordgo.Button{
		Label:    "Join",
		Style:    discordgo.SuccessButton,
		CustomID: "join_game",
		Emoji:    discordgo.ComponentEmoji{Name: "👍"},
	}

	startGameButton := discordgo.Button{
		Label:    "Start Game (Host Only)",
		Style:    discordgo.SuccessButton,
		CustomID: "start_game",
		Emoji:    discordgo.ComponentEmoji{Name: "🎮"},
	}

	cancelGameButton := discordgo.Button{
		Label:    "Cancel Game (Host Only)",
		Style:    discordgo.DangerButton,
		CustomID: "cancel_game",
		Emoji:    discordgo.ComponentEmoji{Name: "🚫"},
	}

	actionRow := discordgo.ActionsRow{
		Components: []discordgo.MessageComponent{
			joinButton,
			startGameButton,
			cancelGameButton,
		},
	}

	embed := discordgo.MessageEmbed{
		Title: guild.Name + " against Waxed Paper Rectangles",
		Description: "A game of Cards Against Humanity is starting!\n" +
			"Use `" + activationSigil + "cah join` to join the game.\n" +
			"Use `" + activationSigil + "cah start` to start the game.\n" +
			"Use `" + activationSigil + "cah leave` to leave the game.\n" +
			"Use `" + activationSigil + "cah end` to end the game.\n" +
			"Use `" + activationSigil + "cah status` to get the current game status.\n" +
			"Use `" + activationSigil + "cah points` to set the number of points to win.\n" +
			"Use `" + activationSigil + "cah decks` to set the decks to use.\n" +
			"Use `" + activationSigil + "cah reset` to reset the game.\n",
		Color: 0x00ff00,
	}

	messageSend := discordgo.MessageSend{
		Embeds: []*discordgo.MessageEmbed{
			&embed,
		},
		Components: []discordgo.MessageComponent{
			actionRow,
		},
	}

	// Send a message to the channel saying the game has started
	_, err := c.SendComplexMessage(s, m, i, &messageSend, false)

	if err != nil {
		log.Printf("Start: Error sending message: %s", err.Error())
	}

	message, err := s.ChannelMessageSend(channelId, "Joined Users: "+strings.Join(c.MentionPlayerNames(), ", "))

	if err != nil {
		log.Printf("Start: Error sending message: %s", err.Error())
		return err
	}

	log.Printf("Message ID: %s", message.ID)

	c.DiscordMessageIds["joined_players"] = message.ID

	return err

}
