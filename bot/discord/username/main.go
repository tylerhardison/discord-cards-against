package usernameGenerator

import (
	"context"
	"fmt"
	"os"

	"github.com/sashabaranov/go-openai"
)

func CreateUsernames(username string, prompt string) string {
	// Strip new lines from api key
	key := os.Getenv("OPENAI_API_KEY")
	if len(key) > 0 && key[len(key)-1] == '\n' {
		key = key[:len(key)-1]
	}
	client := openai.NewClient(key)

	req := openai.ChatCompletionRequest{
		Model: openai.GPT4,
		Messages: []openai.ChatCompletionMessage{
			{
				Role:    openai.ChatMessageRoleSystem,
				Content: fmt.Sprintf("Generate a series of no more than ten phenomenally creative usernames for %s who offered this as their prompt: %s", username, prompt),
			},
		},
	}

	resp, err := client.CreateChatCompletion(context.Background(), req)

	if err != nil {
		fmt.Printf("ChatCompletion error: %v\n", err)
		return ""
	}

	return resp.Choices[0].Message.Content
}
