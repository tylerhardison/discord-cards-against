package cardsagainst

import (
	"strconv"
	"strings"
)

func (c *CardsAgainst) GetNextInput() DiscordInput {
	// Get the next input from the buffer
	c.mux.Lock()
	defer c.mux.Unlock()
	if len(c.GameState.DiscordInput) == 0 {
		return DiscordInput{}
	}
	returnString := c.GameState.DiscordInput[0]

	// Remove the input from the buffer
	c.GameState.DiscordInput = c.GameState.DiscordInput[1:]

	return returnString
}

func messageIsNumber(message string) bool {
	// Check if the message is a number
	// It can be number
	// or number, number
	// or number, number, number
	// or "number number"
	// or "number number number"

	// Does the string have commas?
	if strings.Contains(message, ",") {
		// Split the string by commas
		cardNumbers := strings.Split(message, ",")
		// Check if the user submitted #, #
		for _, cardNumber := range cardNumbers {
			cardNumber = strings.TrimSpace(cardNumber)
			actualNumber, err := strconv.Atoi(cardNumber)
			if err != nil {
				return false
			}
			if actualNumber < 1 || actualNumber > 10 {
				return false
			}
		}
		return true
	} else if strings.Contains(message, " ") {
		// Split the string by spaces
		cardNumbers := strings.Split(message, " ")
		// Check if the user submitted #, #
		for _, cardNumber := range cardNumbers {
			cardNumber = strings.TrimSpace(cardNumber)
			actualNumber, err := strconv.Atoi(cardNumber)
			if err != nil {
				return false
			}
			if actualNumber < 1 || actualNumber > 10 {
				return false
			}
		}
		return true
	}

	_, err := strconv.Atoi(message)
	return err == nil
}
