package cardsagainst

import (
	"fmt"
	"log"
	"strconv"

	"github.com/bwmarrin/discordgo"
)

const (
	DefaultPointsToWin = 5
)

func (c *CardsAgainst) SetGamePoints(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {

	// Set the number of points to win

	c.mux.Lock()
	defer c.mux.Unlock()

	var points int
	points = DefaultPointsToWin
	// Configure the game of Cards Against Humanity
	// We need to get the number of points to win
	// The number of points to win is the first argument
	// If no arguments are passed, we default to 5 points to win

	if c.GameState.CurrentState != WaitingForPlayers {
		_, err := c.SendResponse(s, m, i, []string{"Score can only be set during the initial game configuration! (Hint: while waiting for players to join)"}, true)
		if err != nil {
			log.Printf("Points: Error sending message: %s", err.Error())
		}
		return err
	}

	var err error

	if len(arguments) == 0 {
		_, err = c.SendResponse(s, m, i, []string{fmt.Sprintf("The game has been configured to %d points to win!", points)}, true)
		if err != nil {
			log.Printf("Points: Error sending message: %s", err.Error())
		}
		return err
	}

	if len(arguments) > 0 {
		points, err = strconv.Atoi(arguments[0])
		if err != nil {
			_, err = c.SendResponse(s, m, i, []string{"Error parsing number of points: " + err.Error()}, true)
			if err != nil {
				log.Printf("Points: Error sending message: %s", err.Error())
			}
			return err
		}
	}

	// Set the number of points to win
	c.GameState.PointsToWin = points

	_, err = c.SendResponse(s, m, i, []string{fmt.Sprintf("The game has been configured to %d points to win!", points)}, false)

	if err != nil {
		log.Printf("Points: Error sending message: %s", err.Error())
	}

	return err
}
