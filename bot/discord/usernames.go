package discord

import (
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
	usernameGenerator "github.com/mchineboy/cahbot/bot/discord/username"
)

func (b *Bot) Username(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {
	var message *discordgo.Message
	var err error
	channelId := b.GetChannelId(m, i)

	user, err := b.GetUser(s, m, i)

	if err != nil {
		log.Printf("Username: Error getting user: %s", err.Error())
	}

	if i != nil {
		// Respond to InteractionCreate event
		response := discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "Consulting the Oracle",
				Embeds: []*discordgo.MessageEmbed{
					{
						Title: "Consulting the Oracle",
						Fields: []*discordgo.MessageEmbedField{
							{
								Name: "Please Wait...",
							},
						},
						Thumbnail: &discordgo.MessageEmbedThumbnail{
							URL: "https://media.giphy.com/media/UyRnjWSkikBTU2SMOm/giphy.gif",
						},
					},
				},
			},
		}

		err := s.InteractionRespond(i.Interaction, &response)

		if err != nil {
			log.Printf("Error responding to interaction: %s", err.Error())
			return err
		}
	} else if m != nil {
		embed := &discordgo.MessageEmbed{
			Title: "Consulting the Oracle",
			Fields: []*discordgo.MessageEmbedField{
				{
					Name: "Please Wait...",
				},
			},
			Thumbnail: &discordgo.MessageEmbedThumbnail{
				URL: "https://media.giphy.com/media/UyRnjWSkikBTU2SMOm/giphy.gif",
			},
		}

		messageSend := discordgo.MessageSend{
			Embed: embed,
		}

		// Send announcement message
		message, err = s.ChannelMessageSendComplex(
			channelId,
			&messageSend,
		)

		if err != nil {
			log.Printf("Horoscope: Error sending message: %s", err.Error())
			return err
		}
	}

	// Appear to be typing
	err = s.ChannelTyping(channelId)

	if err != nil {
		log.Printf("Username: Error sending message: %s", err.Error())
	}
	usernames := usernameGenerator.CreateUsernames(user.Username, strings.Join(arguments, " "))

	_, err = s.ChannelMessageSend(channelId, usernames)

	if err != nil {
		log.Printf("Username: Error sending message: %s", err.Error())
	}

	if message != nil {
		err = s.ChannelMessageDelete(channelId, message.ID)

		if err != nil {
			log.Printf("Error deleting message: %s", err.Error())
			return err
		}
	}

	return err
}
