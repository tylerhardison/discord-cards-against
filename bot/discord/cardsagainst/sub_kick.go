package cardsagainst

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

func (c *CardsAgainst) KickPlayerCommand(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, discordID string) (bool, error) {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.IsInPlayingState() && c.GameState.Host.DiscordID == discordID {
		_, err := c.SendResponse(s, m, i, []string{"You cannot kick the host."}, true)
		if err != nil {
			log.Printf("KickPlayerCommand: Error sending message: %s", err.Error())
		}
		return false, err
	}

	// This is broken
	// if !c.IsInPlayingState() || !c.IsGameHost(Player{DiscordID: discordID}) {
	// 	_, err := c.SendResponse(s, m, i, []string{"You are not the host."}, true)
	// 	if err != nil {
	// 		log.Printf("KickPlayerCommand: Error sending message: %s", err.Error())
	// 	}
	// 	return false, err
	// }

	for index, player := range c.GameState.Players {
		if player.DiscordID == discordID {
			c.GameState.Players = append(c.GameState.Players[:index], c.GameState.Players[index+1:]...)
			_, err := c.SendResponse(s, m, i, []string{"<@" + discordID + "> has been kicked from the game."}, true)

			if err != nil {
				log.Printf("KickPlayerCommand: Error sending message: %s", err.Error())
			}
			return true, err
		}
	}

	_, err := c.SendResponse(s, m, i, []string{"Player not found."}, true)
	if err != nil {
		log.Printf("KickPlayerCommand: Error sending message: %s", err.Error())
	}
	return false, err
}
