package cardsagainst

import "github.com/bwmarrin/discordgo"

func (c *CardsAgainst) EndGameCommand(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate) (bool, error) {
	c.mux.Lock()
	defer c.mux.Unlock()
	var discordID string

	if m != nil {
		discordID = m.Author.ID
	} else if i != nil {
		discordID = i.Member.User.ID
	}

	if !c.IsInPlayingState() || !c.IsGameHost(Player{DiscordID: discordID}) {
		return false, nil
	}

	c.GameState.CurrentState = DeclareGameWinner

	return true, nil
}
